import * as Model from "../model";
import initialize from "../initialize";

Parse.Cloud.beforeSave(Model.User, async req => {
  req = initialize.initAcl(req);
  let user = req.object as Model.User;
  if (!user.existed()) {
    let count = await new Parse.Query(Model.User).count({ useMasterKey: true });
    if (count > 0) throw "Суперпользователь уже существует";
  }
});
