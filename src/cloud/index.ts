import * as role from './role';
import * as user from './user';
import * as excel from './excel';

export default {
  role,
  user,
  excel
};
