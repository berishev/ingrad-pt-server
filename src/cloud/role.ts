import * as Model from "../model";
import initialize from "../initialize";
Parse.Cloud.beforeSave(Model.Role, async req => {
  req = initialize.initAcl(req);
});
