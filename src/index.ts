import * as Express from 'express';
import * as ringle from 'berish-ringle';
import * as Parse from 'parse';
import * as Http from 'http';
import * as Https from 'https';
import * as ParseDashboard from 'parse-dashboard';
import config from './config';
import abstract from './abstract';
import { ILogger, LoggerTypeEnum } from './abstract/logger';

class Main {
  public app: Express.Express;
  public parseServer: any;
  public server: Https.Server | Http.Server;
  public parseDashboard: ParseDashboard;
  public logger: ILogger;

  constructor() {
    this.logger = abstract.Logger(LoggerTypeEnum.SERVER);
    this.parseDashboard = new ParseDashboard(config.dashboard.config, config.dashboard.options);
    this.logger.info('Parse Dashboard configurated.');
    this.parseServer = abstract.server.createParseServer(config.parse);
    this.logger.info('Parse Server configurated.');
    this.app = Express();
    this.server = abstract.server.createServer(this.app);
    this.logger.info('Server initialized!');
  }

  listen() {
    let self = this;
    return new Promise<void>((resolve, reject) =>
      self.server.listen(config.parse.port, () => {
        resolve(self.logger.info('STARTED!'));
      })
    );
  }

  async run() {
    this.logger.info('Server running...');
    abstract.server.configurateServer(this.app, this.parseServer, this.parseDashboard);
    this.logger.info(`Server mounted at http:/127.0.0.1:${config.parse.port}`);
    this.logger.info(`Parse mounted at ${config.parse.serverURL}`);
    this.logger.info(`Dashboard mounted at http:/127.0.0.1:${config.parse.port + config.dashboard.mount}`);
    await this.listen();
  }
}

export default ringle.getSingleton(Main);
