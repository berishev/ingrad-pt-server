import * as collection from 'berish-collection';
import * as Logger from 'log';

export enum LoggerTypeEnum {
  CONFIG,
  CLOUD,
  MODEL,
  TRIGGER,
  SERVER,
  COMMAND,
  CUSTOM
}

export interface ILogger {
  debug(str: string);
  info(str: string);
  error(str: string);
}

const loggers = new collection.Dictionary<LoggerTypeEnum, ILogger>();

export default function getLogger(loggerType: LoggerTypeEnum) {
  if (!loggers.containsKey(loggerType))
    loggers.add(
      loggerType,
      new Logger(`[${LoggerTypeEnum[loggerType].toUpperCase()}]\t`)
    );
  return loggers.get(loggerType);
}
