import * as path from 'path';

export function getResourceFileUrl(name: string) {
  return path.join(__dirname, '../../../', 'resources', name);
}
