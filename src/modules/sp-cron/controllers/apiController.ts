import * as Parse from "parse/node";
import * as ringle from "berish-ringle";
import * as sprequest from "sp-request";
import configController from "./configController";

class ApiController {
  sharepoint: sprequest.ISPRequest = null;

  init() {
    this.initParse();
    this.initSP();
    return this;
  }

  private initParse() {
    Parse.initialize(
      configController.app.api.appId,
      configController.app.api.jsKey
    );
    Parse!.serverURL = configController.app.api.serverURL;
  }

  private initSP() {
    this.sharepoint = sprequest.create({
      username: configController.app.sp.username,
      password: configController.app.sp.password
    });
  }

  method(methodName: string, params?: { [key: string]: any }) {
    let paramsUrl = "";
    if (params) {
      let keys = Object.keys(params);
      if (keys.length > 0) {
        paramsUrl = keys.map(m => `${m}=${params[m]}`).join("&");
      }
    }
    const url = `${configController.app.sp.serverURL}${
      configController.app.sp.RESTPoint
    }${methodName}${paramsUrl ? "?" + paramsUrl : ""}`;
    return this.sharepoint.get(url);
  }

  async list<T = any>(
    id: string,
    top: number = 1000,
    select: string[] = ["ID"]
  ) {
    const params = {
      "@v0": `guid'${id}'`,
      $top: top,
      $select: select.join(",")
    };
    const response = await this.method(`Lists(@v0)/Items`, params);
    const body = response && response.body;
    return body && body.d && (body.d.results as Array<T>);
  }
}

export default ringle.getSingleton(ApiController);
