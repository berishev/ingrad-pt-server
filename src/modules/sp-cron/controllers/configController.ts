import * as ringle from "berish-ringle";

interface IConfigInit {
  api: {
    appId: string;
    jsKey: string;
    masterKey: string;
    serverURL: string;
  };
  sp: {
    username: string;
    password: string;
    serverURL: string;
    RESTPoint: string;
  };
}

class ConfigController {
  private _config: IConfigInit = null;

  init(config: IConfigInit) {
    this._config = config;
  }

  get app() {
    return this._config;
  }
}

export default ringle.getSingleton(ConfigController);
