import * as ringle from "berish-ringle";
import { LINQ } from "berish-linq";
import * as Parse from "parse/node";
import apiController from "../controllers/apiController";

import * as SPUserModel from "./spUserModel";
import * as SPOrgStructureModel from "./spOrgStructureModel";

import * as Model from "../model";

class SPController {
  async getUsers() {
    let items = await apiController.list<SPUserModel.Model>(
      SPUserModel.guidName,
      SPUserModel.top,
      SPUserModel.fields
    );
    return LINQ.fromArray(items);
  }

  async getOrgStructures() {
    let items = await apiController.list<SPOrgStructureModel.Model>(
      SPOrgStructureModel.guidName,
      SPOrgStructureModel.top,
      SPOrgStructureModel.fields
    );
    return LINQ.fromArray(items);
  }

  async getParseUsers() {
    let query = new Parse.Query(Model.SPUser).limit(1000);
    let items = await query.find();
    return LINQ.fromArray(items);
  }

  async getParseOrgStructures() {
    let query = new Parse.Query(Model.SPOrgStructure).limit(1000);
    let items = await query.find();
    return LINQ.fromArray(items);
  }

  toSPUserModel(
    raw: { from: SPUserModel.Model; departament: Model.SPOrgStructure },
    to: Model.SPUser
  ) {
    const { from, departament } = raw;
    to.spID = from.Id;
    to.login = from.Login;
    to.firstname = from.Firstname;
    to.lastname = from.Lastname;
    to.patronymic = from.Patronymic;
    to.photo = from.Photo;
    to.position = from.Position;
    to.phoneWork = from.PhoneWork;
    to.personalPhone = from.PersonalPhone;
    to.email = from.Email;
    to.idonec = from.IDOneC;
    to.snils = from.SNILS;
    to.inn = from.INN;
    to.organization = from.Organiation;
    to.currentEmployee = from.CurrentEmployee;
    to.interests = from.Interests;
    to.sphere = from.Sphere;
    to.office = from.Office;
    to.sex =
      from.Sex == "F" ? Model.SPUserSexEnum.female : Model.SPUserSexEnum.male;
    to.personalEmail = from.PersonalEmail;
    to.otherContact = from.OtherContact;
    to.media = from.Media;
    to.articles = from.articles;
    to.skills = from.Skills;
    to.phoneWorker = from.PhoneWorker;
    to.phoneAdditional = from.PhoneAdditional;
    to.department = departament;
    to.isArchive = from.IsArchive;
    to.hireDate = from.HireDate && new Date(from.HireDate);
    to.birthday = from.Birthday && new Date(from.Birthday);
    to.hidetel = from.hidetel;
    to.displayOnPortal = from.DisplayOnPortal == "true";
    to.floorNumber = from.FloorNumber;
    to.LinkTitle = from.LinkTitle;
    return to;
  }

  toSPOrgStructure(from: SPOrgStructureModel.Model, to: Model.SPOrgStructure) {
    to.spID = from.Id;
    to.priority = from.Priority;
    to.isArchive = from.IsArchive;
    to.parentSPID = from.ParentId;
    to.LinkTitle = from.LinkTitle;
    to.idEmployees = +from.IdEmployees;
    to.assist = from.assist;
    return to;
  }
}

export default ringle.getSingleton(SPController);
