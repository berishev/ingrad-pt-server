export const guidName = "74bf5091-9102-4a58-b170-f3365479d223";
export const top = 10000;
export const fields = [
  "ID",
  "ParentId",
  "LinkTitle",
  "Priority",
  "IsArchive",
  "IdEmployees",
  "assist"
];

export interface Model {
  Id: number;
  Priority: number;
  IsArchive: boolean;
  ParentId: number;
  LinkTitle: string;
  IdEmployees: string;
  assist: number;
}
