import config from '../parse';

const users = [{ user: 'admin', pass: 'admin' }];

let apps = [config].map(m => {
  return {
    serverURL: m.publicServerURL,
    appId: m.appId,
    masterKey: m.masterKey,
    javascriptKey: m.javascriptKey,
    restKey: m.restAPIKey,
    appName: m.appName,
    production: m.production
  };
});

export default {
  config: {
    apps,
    users
  },
  options: {
    allowInsecureHTTP: true,
    cookieSessionSecret: config.appId
  },
  mount: '/dashboard'
};
