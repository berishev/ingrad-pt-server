import * as fs from 'fs';
import { getResourceFileUrl } from '../../abstract/resource';

const sslPath = getResourceFileUrl('gis.ingrad.com.pfx');

export default {
  pfx: fs.readFileSync(sslPath),
  passphrase: "123456"
};
