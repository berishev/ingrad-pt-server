import base from "./base";
import { IParseConfig } from "../interfaces/parse";
export default {
  ...base,
  serverURL: `https://127.0.0.1:${base.port}${base.mountPath}`,
  publicServerURL: `https://127.0.0.1:${base.port}${base.mountPath}`
} as IParseConfig;
