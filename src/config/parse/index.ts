import { IParseConfig } from '../interfaces/parse';

export function importConfig(config?: string) {
  if (!config) config = process.env.NODE_ENV || 'development';
  const depPath = `./${config}`;
  const dep = require(depPath);
  return ((dep && dep.default) || dep || null) as IParseConfig;
}

export default importConfig();