import base from "./base";
import { IParseConfig } from "../interfaces/parse";

export default {
  ...base,
  // serverURL: `https://127.0.0.1:${base.port}${base.mountPath}`,
  serverURL: `https://gis.ingrad.com:${base.port}${base.mountPath}`,
  // databaseURI: 'mongodb://localhost:27017/gk-pt-db',
  publicServerURL: `https://gis.ingrad.com:${base.port}${base.mountPath}`,
  production: true
} as IParseConfig;
