import * as Model from './';

export class TeamUser extends Parse.Object {
    constructor(){
        super('TeamUser')
    }

    get team() {
        return this.get('team')
    }

    set team(value: Model.Team) {
        this.set('team', value)
    }

    get position() {
        return this.get('position')
    }

    set position(value: string) {
        this.set('position', value)
    }

    get task() {
        return this.get('task')
    }

    set task(value: string) {
        this.set('task', value)
    }

    get lastname() {
        return this.get('lastname')
    }

    set lastname(value: string) {
        this.set('lastname', value)
    }

    get name() {
        return this.get('name')
    }

    set name(value: string) {
        this.set('name', value)
    }

    get patronymic() {
        return this.get('patronymic')
    }

    set patronymic(value: string) {
        this.set('patronymic', value)
    }

    get phoneCode() {
        return this.get('phoneCode')
    }

    set phoneCode(value: number) {
        this.set('phoneCode', value)
    }

    get phoneMobile() {
        return this.get('phoneMobile')
    }

    set phoneMobile(value: string) {
        this.set('phoneMobile', value)
    }

    get officeNumber() {
        return this.get('officeNumber')
    }

    set officeNumber(value: string) {
        this.set('officeNumber', value)
    }

    get floorOrRoom() {
        return this.get('floorOrRoom')
    }

    set floorOrRoom(value: string) {
        this.set('floorOrRoom', value)
    }

    get projects() {
        let value = this.get('projects');
        if(!value) this.set('projects', []);
        return this.get('projects')
    }

    set projects(value: Model.Project[]) {
        this.set('projects', value)
    }
}

Parse.Object.registerSubclass('TeamUser', TeamUser);