export enum ProjectRelTypeEnum {
  M,
  MO
}

export class Project extends Parse.Object {
  constructor() {
    super('Project');
  }

  get code() {
    return this.get('code');
  }

  set code(value: string) {
    this.set('code', value);
  }

  get name() {
    return this.get('name');
  }

  set name(value: string) {
    this.set('name', value);
  }

  get commercialName() {
    return this.get('commercialName');
  }

  set commercialName(value: string) {
    this.set('commercialName', value);
  }

  get iconUrl() {
    return this.get('iconUrl');
  }

  set iconUrl(value: string) {
    this.set('iconUrl', value);
  }

  get relType() {
    return ProjectRelTypeEnum[this.get('relType') as string];
  }

  set relType(value: ProjectRelTypeEnum) {
    this.set('relType', ProjectRelTypeEnum[value]);
  }
}

Parse.Object.registerSubclass('Project', Project);
